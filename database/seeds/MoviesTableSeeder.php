<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Movie::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Movie::create([
                'title' => $faker->catchPhrase,
                'rating' => $faker->numberBetween(1, 10) . "/10",
                'description' => $faker->paragraph,
                'image_url' => $faker->imageUrl(640, 480, 'cats'),
                'genre_id' => $faker->numberBetween(1, 10),
            ]);
        }
    }
}
