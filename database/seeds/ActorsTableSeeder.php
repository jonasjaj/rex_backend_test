<?php

use Illuminate\Database\Seeder;
use App\Actor;

class ActorsTableSeeder extends Seeder
{
    /**
     * Create a random date of birth.
     *
     * @return string
     */
    public function createDOB()
    {
        $day = rand(1, 31);
        $month = rand(1, 12);
        $year = rand(1950, 1997);
        return (string)$day . "-" . (string)$month . "-" . (string)$year;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Actor::truncate();

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $dob = $this->createDOB();
            $age = date_diff(date_create($dob), date_create('today'))->y;

            Actor::create([
                'name' => $faker->name,
                'date_of_birth' => $dob,
                'age' => $age,
                'biography' => $faker->text,
                'image_url' => $faker->imageUrl(640, 480, 'cats'),
            ]);
        }
    }
}
