<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = \Faker\Factory::create();

        $password = Hash::make('secret');

        # Create admin user
        User::create([
            'name' => 'Admin',
            'email' => 'admin@rexsoftware.com',
            'password' => $password,
        ]);

        for ($i = 0; $i < 9; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
            ]);
        }
    }
}
