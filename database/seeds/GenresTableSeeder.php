<?php

use Illuminate\Database\Seeder;
use App\Genre;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::truncate();

        $genres = [
            'Drama',
            'Comedy',
            'Thriller',
            'Horror',
            'Action',
            'Anime',
            'Adventure',
            'Documentary',
            'Western',
            'Fantasy',
        ];

        for ($i = 0; $i < 10; $i++) {
            Genre::create([
                'name' => $genres[$i],
            ]);
        }
    }
}
