<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        $faker = \Faker\Factory::create();

        $count = 1;

        # Give each movie three roles
        for ($i = 0; $i < 30; $i++) {
            Role::create([
                'character' => $faker->firstName,
                'actor_id' => $faker->numberBetween(1, 20),
                'movie_id' => $count,
            ]);
            $count == 10 ? $count = 1 : $count++;
        }
    }
}
