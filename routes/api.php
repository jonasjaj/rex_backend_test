<?php

use Illuminate\Http\Request;
use App\Actor;
use App\Genre;
use App\Movie;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('movies/{movie}/favorite', 'MovieController@favorite');
    Route::resource('movies', 'MovieController');
    Route::resource('actors', 'ActorController');
    Route::resource('genres', 'GenreController');
});

Route::get('/', 'HomeController@api');

# Auth routes
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
Route::post('register', 'Auth\RegisterController@register');
