FORMAT: 1A

# Movie API
Movie API is a simple API created by Jonas Anker Jahr that allows users to read and write data
about movies, actors and genres, and the great people at Rex Software to evaluate his Laravel skills.

# Group Quickstart

### Login [POST /api/login]
Start out by logging in and retrieving an api_token.
+ Request (application/json)
```
    {
        "email": "admin@rexsoftware.com",
        "password": "secret"
    }
```
+ Response 200 (application/json)
```
    {
        "id": "1",
        "name": "Admin",
        "email": "admin@rexsoftware.com",
        "created_at": "2017-09-11 02:28:35",
        "updated_at": "2017-09-11 03:06:57",
        "api_token": "Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5"
    }
```

### Using Endpoints
+ To use the other endpoints we need to include the api_token as a header or as a payload to avoid getting a `401`.
    + Header: `Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5`
    + Payload: `{ "api_token": "Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5" }`

# Group Movies

## Movies Collection [/api/movies]

### List All Movies [GET]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "title": "Open-architected multi-tasking strategy",
        "rating": "9/10",
        "description": "Aut consectetur sit delectus quo. Est minima exercitationem saepe itaque vel.",
        "image_url": "https://lorempixel.com/640/480/cats/?33746",
        "genre": {
            "id": "8",
            "name": "Documentary"
        },
        "roles": [
            {
                "actor": {
                    "id": "12",
                    "name": "Katlynn Sauer"
                },
                "character": "Hannah"
            },
            {
                "actor": {
                    "id": "18",
                    "name": "Oswald Hansen"
                },
                "character": "Marge"
            },
            {
                "actor": {
                    "id": "12",
                    "name": "Katlynn Sauer"
                },
                "character": "Quinten"
            }
        ]
    }
```

### Retrieve Movie [GET /api/movies/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "title": "Open-architected multi-tasking strategy",
        "rating": "9/10",
        "description": "Aut consectetur sit delectus quo. Est minima exercitationem saepe itaque vel.",
        "image_url": "https://lorempixel.com/640/480/cats/?33746",
        "genre": {
            "id": "8",
            "name": "Documentary"
        },
        "roles": [
            {
                "actor": {
                    "id": "12",
                    "name": "Katlynn Sauer"
                },
                "character": "Hannah"
            },
            {
                "actor": {
                    "id": "18",
                    "name": "Oswald Hansen"
                },
                "character": "Marge"
            },
            {
                "actor": {
                    "id": "12",
                    "name": "Katlynn Sauer"
                },
                "character": "Quinten"
            }
        ]
    }
```

### Create Movie [POST /api/movies]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        {
            "title": "Your Moms Favorite",
            "rating": "9/10",
            "description": "The best movie ever. Your mom loves it. You need to love it too.",
            "image_url": "https://lorempixel.com/640/480/cats/?45560",
            "genre_id": "4"
        }
    ```
+ Response 201 (application/json)
```
    {
        "title": "Your Moms Favorite",
        "rating": "9/10",
        "description": "The best movie ever. Your mom loves it. You need to love it too.",
        "image_url": "https://lorempixel.com/640/480/cats/?45560",
        "genre_id": "4",
        "updated_at": "2017-09-11 03:14:44",
        "created_at": "2017-09-11 03:14:44",
        "id": "13"
    }
```

## Update Movie [PATCH or PUT /api/movies/{id}]
PATCH is used for n-number of fields and PUT for all fields.
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        {
            "title": "Your Dads Favorite",
            "rating": "6/10",
            "description": "The best movie ever. Your dad loves it. You need to love it too.",
            "image_url": "https://lorempixel.com/640/480/cats/?45560",
            "genre_id": "2"
        }
    ```
+ Response 200 (application/json)
```
    {
        "title": "Your Dads Favorite",
        "rating": "6/10",
        "description": "The best movie ever. Your dad loves it. You need to love it too.",
        "image_url": "https://lorempixel.com/640/480/cats/?45560",
        "genre_id": "2",
        "updated_at": "2017-09-11 03:14:44",
        "created_at": "2017-09-11 03:14:44",
        "id": "13"
    }
```

## Favorite Movie [POST /api/movies/{id}/favorite]
A logged in user can favorite a movie.
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        {
            "user_id": 1,
            "movie_id": 4,
            "updated_at": "2017-09-11 04:14:43",
            "created_at": "2017-09-11 04:14:43",
            "id": 1
        },
        "message": "Movie has been favorited."
    }    
```

### Delete Movie [DELETE /api/movies/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    { "message" => "Movie with id = {id} was successfully deleted." }
```

# Group Actors

## Actors Collection [/api/actors]

### List All Actors [GET]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "name": "Kacey Metz",
        "date_of_birth": "17-2-1964",
        "age": 53,
        "biography": "Et aut nobis explicabo vel rem. Eos et dolor quod rerum natus voluptatem. Error voluptatum quia id earum.",
        "image_url": "https://lorempixel.com/640/480/cats/?48929"
    }
```

### Retrieve Actor [GET /api/actors/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "name": "Kacey Metz",
        "date_of_birth": "17-2-1964",
        "age": 53,
        "biography": "Et aut nobis explicabo vel rem. Eos et dolor quod rerum natus voluptatem. Error voluptatum quia id earum.",
        "image_url": "https://lorempixel.com/640/480/cats/?48929"
    }
```

### Create Actor [POST /api/actors]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        {
            "name": "Jonas Anker Jahr",
            "date_of_birth": "22-12-1994",
            "age": "22",
            "biography": "This is my awesome biography. I like guitars and programming.",
            "image_url": "https://www.w3schools.com/w3images/fjords.jpg"
        }
    ```
+ Response 201 (application/json)
```
    {
        "name": "Jonas Anker Jahr",
        "date_of_birth": "22-12-1994",
        "age": "22",
        "biography": "This is my awesome biography. I like guitars and programming.",
        "image_url": "https://www.w3schools.com/w3images/fjords.jpg",
        "updated_at": "2017-09-11 03:38:24",
        "created_at": "2017-09-11 03:38:24",
        "id": 23
    }
```

## Update Actor [PATCH or PUT /api/actors/{id}]
PATCH is used for n-number of fields and PUT for all fields.
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        {
            "name": "Jonas Anker JaarrRRgHH",
            "date_of_birth": "22-12-1994",
            "age": "22",
            "biography": "I am a pirateeeeee.",
            "image_url": "https://www.w3schools.com/w3images/fjords.jpg"
        }
    ```
+ Response 200 (application/json)
```
    {
        "id": 23,
        "name": "Jonas Anker JaarrRRgHH",
        "date_of_birth": "22-12-1994",
        "age": "22",
        "biography": "I am a pirateeeeee.",
        "image_url": "https://www.w3schools.com/w3images/fjords.jpg",
        "created_at": "2017-09-11 03:38:24",
        "updated_at": "2017-09-11 03:42:50"
    }
```

### Delete Actor [DELETE /api/actors/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    { "message" => "Actor with id = {id} was successfully deleted." }
```

# Group Genres

## Genres Collection [/api/genres]

### List All Genres [GET]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "name": "Drama",
        "movies": [
            {
                "id": 7,
                "title": "Centralized attitude-oriented website",
                "rating": "3/10",
                "description": "Laborum saepe illum consequatur. Reprehenderit et aspernatur aperiam enim sed.",
                "image_url": "https://lorempixel.com/640/480/cats/?30023",
                "genre": {
                    "id": 1,
                    "name": "Drama"
                }
            }
        ]
    }
```

### Retrieve Genre [GET /api/genres/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    {
        "id": 1,
        "name": "Drama",
        "movies": [
            {
                "id": 7,
                "title": "Centralized attitude-oriented website",
                "rating": "3/10",
                "description": "Laborum saepe illum consequatur. Reprehenderit et aspernatur aperiam enim sed.",
                "image_url": "https://lorempixel.com/640/480/cats/?30023",
                "genre": {
                    "id": 1,
                    "name": "Drama"
                }
            }
        ]
    }
```

### Create Genre [POST /api/genres]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        { "name": "Steampunk" }
    ```
+ Response 201 (application/json)
```
    {
        "name": "Steampunk",
        "updated_at": "2017-09-11 03:51:38",
        "created_at": "2017-09-11 03:51:38",
        "id": 13
    }
```

## Update Genre [PATCH or PUT /api/genres/{id}]
PATCH is used for n-number of fields and PUT for all fields.
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
    + Body
    ```
        { "name": "Absurdist" }
    ```
+ Response 200 (application/json)
```
    {
        "name": "Absurdist",
        "updated_at": "2017-09-11 03:51:38",
        "created_at": "2017-09-11 03:53:38",
        "id": 13
    }
```

### Delete Genre [DELETE /api/genres/{id}]
+ Parameters
    + id (int)
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    { "message" => "Genre with id = {id} was successfully deleted." }
```

# Group authentication

### Login [POST /api/login]
+ Request (application/json)
```
    {
        "email": "admin@rexsoftware.com",
        "password": "secret"
    }
```
+ Response 200 (application/json)
```
    {
        "id": "1",
        "name": "Admin",
        "email": "admin@rexsoftware.com",
        "created_at": "2017-09-11 02:28:35",
        "updated_at": "2017-09-11 03:06:57",
        "api_token": "Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5"
    }
```

### Logout [POST /api/logout]
+ Request (application/json)
    + Headers
    ```
        Authorization: Bearer Pga6FHBhab8UuVum0G7tOd5VgZOhzNDerrxHaHH2gVoOZB3qsCvlYjdd1MD5
    ```
+ Response 200 (application/json)
```
    { "message" => "User logged out." }
```

### Register
+ Request (application/json)
```
    {
        "name": "Jonas",
        "email": "jonas@rexsoftware.com",
        "password": "secret",
        "password_confirmation": "secret"
    }
```
+ Response 201 (application/json)
```
    {
        "data": {
            "name": "Jonas",
            "email": "jonas@rexsoftware.com",
            "updated_at": "2017-09-11 04:08:40",
            "created_at": "2017-09-11 04:08:40",
            "id": 23,
            "api_token": "G61FqP8w5M7un89LF3Cv9xljq9c4fethyB7dc8guAQ8Ja2mzNUE9FYVsOzHM"
        }
    }
```
