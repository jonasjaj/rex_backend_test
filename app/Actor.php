<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    protected $fillable = ['name', 'date_of_birth', 'age', 'biography', 'image_url'];

    /**
     * Relationships
     */
    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }

    public function roles()
    {
        return $this->hasMany('App\Role');
    }

    /**
     * Transform data output.
     *
     * @return array
     */
    public function transform()
    {
        $actor_object = [
            'id' => $this->id,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'age' => $this->age,
            'biography' => $this->biography,
            'image_url' => $this->image_url,
        ];

        return $actor_object;
    }
}
