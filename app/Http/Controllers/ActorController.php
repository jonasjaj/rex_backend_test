<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Actor;

class ActorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actors = Actor::all();
        $actors_array = [];

        foreach ($actors as $actor) {
            array_push($actors_array, $actor->transform());
        }

        return response()->json($actors_array, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|between:2,30',
            'date_of_birth' => 'required|min:10|max:10',
            'age' => 'required|int',
            'biography' => 'required|between:10,100',
            'image_url' => 'required|url'
        ]);

        $actor = Actor::create([
            'name' => $request->name,
            'date_of_birth' => $request->date_of_birth,
            'age' => $request->age,
            'biography' => $request->biography,
            'image_url' => $request->image_url
        ]);

        return response()->json($actor, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actor = Actor::find($id);
        if ($actor != null) {
            return $actor->transform();
        }
        return response()->json(['message' => 'Actor with id = '.$id.' not found.'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('PUT')) {
            $request->validate([
                'name' => 'required|between:2,30',
                'date_of_birth' => 'required|min:10|max:10',
                'age' => 'required|int',
                'biography' => 'required|between:10,100',
                'image_url' => 'required|url'
            ]);
        }
        elseif ($request->isMethod('PATCH')) {
            $request->validate([
                'name' => 'between:2,30',
                'date_of_birth' => 'min:10|max:10',
                'age' => 'int',
                'biography' => 'between:10,100',
                'image_url' => 'url'
            ]);
        }

        $actor = Actor::find($id);

        $actor->update([
            'name' => $request->name ? $request->name : $actor->name,
            'date_of_birth' => $request->date_of_birth ? $request->date_of_birth : $actor->date_of_birth,
            'age' => $request->age ? $request->age : $actor->age,
            'biography' => $request->biography ? $request->biography : $actor->biography,
            'image_url' => $request->image_url ? $request->image_url : $actor->image_url,
        ]);

        return response()->json($actor, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $actor = Actor::find($id);
        if ($actor != null) {
            $actor->delete();
            return response()->json(['message' => 'Actor with id = '.$id.' was successfully deleted.'], 200);
        }
        return response()->json(['message' => 'Actor with id = '.$id.' was not found.'], 404);
    }
}
