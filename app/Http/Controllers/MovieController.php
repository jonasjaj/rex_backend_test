<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Movie;
use App\Role;
use App\User;
use App\Favorite;

class MovieController extends Controller
{
    /**
     * Favorite a movie.
     *
     * @return \Illuminate\Http\Response
     */
    public function favorite(Request $request, $id)
    {
        $movie = Movie::find($id);
        if ($movie == null) {
            return response()->json('Movie does not exist.', 404);
        }
        $token = str_replace('Bearer ', '', $request->header('Authorization'));
        $user = User::where('api_token', $token)->first();
        $favorite = Favorite::where('user_id', $user->id)->where('movie_id', $movie->id)->first();
        if ($favorite != null) {
            return response()->json(['message' => 'Movie has already been favorited.', $favorite], 422);
        }
        $favorite = Favorite::create([
            'user_id' => $user->id,
            'movie_id' => $movie->id
        ]);
        return response()->json(['message' => 'Movie has been favorited.', $favorite], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        $movies_array = [];

        foreach ($movies as $movie) {
            array_push($movies_array, $movie->transform());
        }

        return response()->json($movies_array, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|between:2,50',
            'rating' => 'required|between:4,5',
            'description' => 'required|between:5,100',
            'image_url' => 'required|url',
            'genre_id' => 'required|exists:genres,id',
        ]);

        $movie = Movie::create([
            'title' => $request->title,
            'rating' => $request->rating,
            'description' => $request->description,
            'image_url' => $request->image_url,
            'genre_id' => $request->genre_id,
        ]);

        return response()->json($movie, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = Movie::find($id);
        if ($movie != null) {
            return $movie->transform();
        }
        return response()->json(['message' => 'Movie with id = '.$id.' not found.'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('PUT')) {
            $request->validate([
                'title' => 'required|between:2,50',
                'rating' => 'required|between:4,5',
                'description' => 'required|between:5,100',
                'image_url' => 'required|url',
                'genre_id' => 'required|exists:genres,id',
            ]);
        }
        elseif ($request->isMethod('PATCH')) {
            $request->validate([
                'title' => 'between:2,50',
                'rating' => 'between:4,5',
                'description' => 'between:5,100',
                'image_url' => 'url',
                'genre_id' => 'exists:genres,id',
            ]);
        }

        $movie = Movie::find($id);

        $movie->update([
            'title' => $request->title ? $request->title : $movie->title,
            'rating' => $request->rating ? $request->rating : $movie->rating,
            'description' => $request->description ? $request->description : $movie->description,
            'image_url' => $request->image_url ? $request->image_url : $movie->image_url,
            'genre_id' => $request->genre_id ? $request->genre_id : $movie->genre_id,
        ]);

        return response()->json($movie, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);
        if ($movie != null) {
            $movie->delete();
            return response()->json(['message' => 'Movie with id = '.$id.' was successfully deleted.'], 200);
        }
        return response()->json(['message' => 'Movie with id = '.$id.' was not found.'], 404);
    }
}
