<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * List all endpoints and associated methods
     */
    public function api()
    {
        $endpoints = [
            "version" => "1.0",
            "endpoints" => [
                "authentication" => [
                    [
                        "href" => "/login",
                        "rel" => "login",
                        "method" => "POST"
                    ],
                    [
                        "href" => "/register",
                        "rel" => "register",
                        "method" => "POST"
                    ],
                    [
                        "href" => "/logout",
                        "rel" => "logout",
                        "method" => "POST"
                    ]
                ],
                "actors" => [
                    [
                        "href" => "/actors",
                        "rel" => "list",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/actors/{id}",
                        "rel" => "self",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/actors",
                        "rel" => "create",
                        "method" => "POST"
                    ],
                    [
                        "href" => "/actors/{id}",
                        "rel" => "edit",
                        "method" => "PUT"
                    ],
                    [
                        "href" => "/actors{id}",
                        "rel" => "delete",
                        "method" => "DELETE"
                    ],
                ],
                "genres" => [
                    [
                        "href" => "/genres",
                        "rel" => "list",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/genres/{id}",
                        "rel" => "self",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/genres",
                        "rel" => "create",
                        "method" => "POST"
                    ],
                    [
                        "href" => "/genres/{id}",
                        "rel" => "edit",
                        "method" => "PUT"
                    ],
                    [
                        "href" => "/genres{id}",
                        "rel" => "delete",
                        "method" => "DELETE"
                    ],
                ],
                "movies" => [
                    [
                        "href" => "/movies",
                        "rel" => "list",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/movies/{id}",
                        "rel" => "self",
                        "method" => "GET"
                    ],
                    [
                        "href" => "/movies",
                        "rel" => "create",
                        "method" => "POST"
                    ],
                    [
                        "href" => "/movies/{id}",
                        "rel" => "edit",
                        "method" => "PUT"
                    ],
                    [
                        "href" => "/movies{id}",
                        "rel" => "delete",
                        "method" => "DELETE"
                    ],
                ]
            ]
        ];
        return response()->json($endpoints, 200);
    }
}
