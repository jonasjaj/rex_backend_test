<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Genre;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $genres = Genre::all();
        $genres_array = [];

        foreach ($genres as $genre) {
            array_push($genres_array, $genre->transform());
        }

        return response()->json($genres_array, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|between:4,20'
        ]);

        $genre = Genre::create([
            'name' => $request->name
        ]);

        return response()->json($genre, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $genre = Genre::find($id);
        if ($genre != null) {
            return $genre->transform();
        }
        return response()->json(['message' => 'Genre with id = '.$id.' not found.'], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('PUT')) {
            $request->validate([
                'name' => 'required|between:4,20'
            ]);
        }
        elseif ($request->isMethod('PATCH')) {
            $request->validate([
                'name' => 'between:4,20'
            ]);
        }

        $genre = Genre::find($id);

        $genre->update([
            'name' => $request->name ? $request->name : $genre->name
        ]);

        return response()->json($genre, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $genre = Genre::find($id);
        if ($genre != null) {
            $genre->delete();
            return response()->json(['message' => 'Genre with id = '.$id.' was successfully deleted.'], 200);
        }
        return response()->json(['message' => 'Genre with id = '.$id.' was not found.'], 404);
    }
}
