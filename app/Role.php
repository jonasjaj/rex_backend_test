<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['character', 'actor_id', 'movie_id'];

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }

    public function actor()
    {
        return $this->belongsTo('App\Actor');
    }
}
