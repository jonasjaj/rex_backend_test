<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = ['user_id', 'movie_id'];

    /**
     * Relationships
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
