<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $fillable = ['name'];

    /**
     * Relationships
     */
    public function movies()
    {
        return $this->hasMany('App\Movie');
    }

    /**
     * Transform data output.
     *
     * @return array
     */
    public function transform()
    {
        $movies_in_genre = [];
        $actors_in_genre = [];

        foreach ($this->movies as $movie) {
            # Add movies in this genre
            $movie_object = [
                'id' => $movie->id,
                'title' => $movie->title,
                'rating' => $movie->rating,
                'description' => $movie->description,
                'image_url' => $movie->image_url,
                'genre' => [
                    'id' => $movie->genre->id,
                    'name' => $movie->genre->name
                ]
            ];
            array_push($movies_in_genre, $movie_object);

            # Add actors in this genre
            foreach ($movie->roles as $role) {
                $actor_object = [
                    'actor' => [
                        'id' => $role->actor->id,
                        'name' => $role->actor->name
                    ]
                ];
                array_push($actors_in_genre, $actor_object);
            }
        }

        $genre_object = [
            'id' => $this->id,
            'name' => $this->name,
            'movies' => $movies_in_genre,
            'actors' => $actors_in_genre
        ];

        return $genre_object;
    }
}
