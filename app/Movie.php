<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Movie extends Model
{
    protected $fillable = ['title', 'rating', 'description', 'image_url', 'genre_id'];

    /**
     * Relationships
     */
    public function actors()
    {
        return $this->hasMany('App\Actor');
    }

    public function genre()
    {
        return $this->belongsTo('App\Genre');
    }

    public function roles()
    {
        return $this->hasMany('App\Role');
    }

    /**
     * Transform data output.
     *
     * @return array
     */
    public function transform()
    {
        $roles_array = [];

        if (count($this->roles) > 0) {
            foreach ($this->roles as $role) {
                $role_object = [
                    'actor' => [
                        'id' => $role->actor->id,
                        'name' => $role->actor->name
                    ],
                    'character' => $role->character
                ];
                array_push($roles_array, $role_object);
            }
        }

        $movie_object = [
            'id' => $this->id,
            'title' => $this->title,
            'rating' => $this->rating,
            'description' => $this->description,
            'image_url' => $this->image_url,
            'genre' => [
                'id' => $this->genre->id,
                'name' => $this->genre->name
            ],
            'roles' => $roles_array
        ];

        return $movie_object;
    }
}
