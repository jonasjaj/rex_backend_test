# Movie API
Movie API is a simple API created by Jonas Anker Jahr that allows users to read and write data
about movies, actors and genres, and the great people at Rex Software to evaluate his Laravel skills.

## Setup
+ Run `composer install` to install all dependencies.
+ Edit the .env file to match your configurations.
+ Run `php artisan rex:run` to create migrations, seed the DB, generate an application key, and serve the app in one command.

## What I've Implemented
+ User authentication that exchanges correct credentials for a secure token.
    + All endpoints except root and authentication endpoints requires the user to pass their token as a header to the HTTP request or as a payload.
    + E.g. as header `Authorization: Bearer SK0ewMtCsm8MFjLIYAfHireMe21psRiR6mU9iZkjBFHQAmu1C4hSn3mM6iCd`.
    + E.g. as payload `{ "api_token": "SK0ewMtCsm8MFjLIYAfHireMe21psRiR6mU9iZkjBFHQAmu1C4hSn3mM6iCd" }`.
+ Read / Write API for */api/actors*, */api/genres* and */api/movies*.
    + PATCH for updating one field and PUT for all fields [so image update doesn't have to be done by a PUT request].
    + Proper validation and error handling.
+ Method for uploading images.
+ API Blueprint documentation.
+ Multiple feature tests.
    + Spawn a new terminal window and run `vendor/bin/phpunit` to see the results of 21 tests.
+ A system that allows logged in users to favorite movies.

## What Now?
+ Let's take a look at the different endpoints by sending a `GET` request to `/api`.
+ Send a `POST` request to `/api/login` with `{ "name": "admin@rexsoftware.com", "password": "secret" }` to receive and api_token
that you can use to access the */movies*, */genres* and */actors* endpoints.
+ Check out Movie_API_Blueprint.md included in this project for further steps.

## Thoughts and Decisions
+ When displaying associated actors/movies/genres I chose to include the id of the respective entities so that you can check that I have modeled the database relationships correctly.
+ Instead of using a response transformation library like Fractal, I decided to make a transform() function in the models that replicate the same functionality. E.g. `$this->transform()`.
+ For the image upload I assumed you were looking for file upload (although it wasn't stated), but I decided to implement it by creating an image_url attribute instead.
The reason for this is that I thought it would be overkill to implement the storage system when a direct link could be implemented 10x faster and provide the same functionality.
The user can just send a `PATCH` request with desired image_url to update the image for an actor or movie.
+ How I would have implemented image upload:
    + `php artisan storage:link`
    + `$path = $request->file('actor')->storeAs('actors', $request->id);`
    + `Actor::find($id)->update(['image_path', $path]);`
+ Call me in for an interview. I won't let you down.
