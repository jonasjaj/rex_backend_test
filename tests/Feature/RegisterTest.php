<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class RegisterTest extends TestCase
{
    public function testsUserIsRegisteredCorrectly()
    {
        $payload = [
            'name' => 'Josas',
            'email' => 'jonas@rexsoftware.com',
            'password' => 'secret',
            'password_confirmation' => 'secret'
        ];

        $this->json('POST', 'api/register', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id'
                ]
            ]);

        # Delete user so we can re-run test
        $user = User::where('email', 'jonas@rexsoftware.com')->first();
        $user->delete();
    }

    public function testsRequirePasswordConfirmation()
    {
        $payload = [
            'name' => 'Josas',
            'email' => 'jonas@rexlabs.com',
            'password' => 'secret',
        ];

        $this->json('POST', 'api/register', $payload)
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => [
                    "The password confirmation does not match."
                    ]
                ]
            ]);
    }
}
