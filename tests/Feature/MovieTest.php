<?php

namespace Tests\Feature;
namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MovieTest extends TestCase
{
    public function testsMoviesEndpointWithoutAPIToken()
    {
        $this->json('GET', 'api/movies')
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testsMoviesAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'title' => 'Your Moms Favorite',
            'rating' => '9/10',
            'description' => 'The best movie ever. Your mom loves it. You need to love it too.',
            'image_url' => 'https://lorempixel.com/640/480/cats/?45560',
            'genre_id' => '4'
        ];

        $this->json('POST', 'api/movies', $payload, $headers)
            ->assertStatus(201)
            ->assertJsonStructure([
                'id',
                'title',
                'rating',
                'description',
                'genre_id',
            ]);
    }

    public function testsMoviesAreListedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', 'api/movies', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'title',
                    'rating',
                    'description',
                    'genre',
                    'roles'
                ]
            ]);
    }

    public function testsMoviesAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $movie = Movie::create([
            'title' => 'Some awesome movie',
            'rating' => '10/10',
            'description' => 'This movie is the best movie of the century.',
            'image_url' => 'https://www.w3schools.com/w3images/fjords.jpg',
            'genre_id' => '4'
        ]);

        $this->json('DELETE', 'api/movies/'.$movie->id, [], $headers)
            ->assertStatus(200)
            ->assertJson(['message' => 'Movie with id = '.$movie->id.' was successfully deleted.']);
    }
}
