<?php

namespace Tests\Feature;
namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GenreTest extends TestCase
{
    public function testsActorsEndpointWithoutAPIToken()
    {
        $this->json('GET', 'api/genres')
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testsGenresAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = ['name' => 'Mockumentary'];

        $this->json('POST', 'api/genres', $payload, $headers)
            ->assertStatus(201)
            ->assertJson(['name' => 'Mockumentary']);
    }

    public function testsGenresAreListedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', 'api/genres', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'name'
                ]
            ]);
    }

    public function testsGenresAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $genre = Genre::create([
            'name' => 'My genre'
        ]);

        $this->json('DELETE', 'api/genres/'.$genre->id, [], $headers)
            ->assertStatus(200)
            ->assertJson(['message' => 'Genre with id = '.$genre->id.' was successfully deleted.']);
    }
}
