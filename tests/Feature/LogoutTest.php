<?php

namespace Tests\Feature;
namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
    public function testsUserIsLoggedOutProperly()
    {
        $user = factory(User::class)->create(['email' => 'test@test.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', 'api/movies', [], $headers)
            ->assertStatus(200);
        $this->json('POST', 'api/logout', [], $headers)
            ->assertStatus(200);

        $user = User::find($user->id);
        $this->assertEquals(null, $user->api_token);

        $user->delete(); # Delete user so we can re-run test
    }

    public function testsUserWithNullToken()
    {
        $user = factory(User::class)->create(['email' => 'test@test.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $user->api_token = null;
        $user->save();

        $this->json('GET', 'api/movies', [], $headers)
            ->assertStatus(401);

        $user->delete(); # Delete user so we can re-run test
    }
}
