<?php

namespace Tests\Feature;
namespace App;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActorTest extends TestCase
{
    public function testsActorsEndpointWithoutAPIToken()
    {
        $this->json('GET', 'api/actors')
            ->assertStatus(401)
            ->assertJson([
                'message' => 'Unauthenticated.'
            ]);
    }

    public function testsActorsAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            "name" => "Jonas Anker Jahr",
            "date_of_birth" => "22-12-1994",
            "age" => "22",
            "biography" => "This is my awesome biography. I like guitars and Laravel.",
            "image_url" => "https://www.w3schools.com/w3images/fjords.jpg"
        ];

        $this->json('POST', 'api/actors', $payload, $headers)
            ->assertStatus(201)
            ->assertJson([
                "name" => "Jonas Anker Jahr",
                "date_of_birth" => "22-12-1994",
                "age" => "22",
                "biography" => "This is my awesome biography. I like guitars and Laravel.",
                "image_url" => "https://www.w3schools.com/w3images/fjords.jpg"
            ]);
    }

    public function testsActorsAreListedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('GET', 'api/actors', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    'id',
                    'name',
                    'date_of_birth',
                    'age'
                ]
            ]);
    }

    public function testsActorAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $actor = Actor::create([
            'name' => 'Jonas Anker Jahr',
            'date_of_birth' => '22-12-1994',
            'age' => '22',
            'biography' => 'This is my biography!',
            'image_url' => 'https://www.w3schools.com/w3images/fjords.jpg'
        ]);

        $this->json('DELETE', 'api/actors/'.$actor->id, [], $headers)
            ->assertStatus(200)
            ->assertJson(['message' => 'Actor with id = '.$actor->id.' was successfully deleted.']);
    }
}
